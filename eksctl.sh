################# EKSCTL #################
# Install on Windows
# if you don’t have scoop then type the following command
iwr -useb get.scoop.sh | iex
# download eksctl
scoop install eksctl

# Install on Linux
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin

# Create Cluster
eksctl create cluster --name test --region eu-central-1 --node-type t2.medium --nodes 1
